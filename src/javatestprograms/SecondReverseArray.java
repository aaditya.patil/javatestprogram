package javatestprograms;

public class SecondReverseArray {
	
	//Reverse the array

	public static void main(String[] args) {
		
		int []array = {11,19,15,10};
		 
        System.out.println("Original array");
       
        for(int i=0; i<array.length;i++)
        {
            System.out.print(array[i] + " ");
        }
        
        System.out.println();
        
        System.out.println("Reverse Array is");
       
        for(int i= array.length-1;i>= 0; i--)
        {
            System.out.print(array[i] + " ");

	}

	}
}
