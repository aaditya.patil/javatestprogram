package javatestprograms;

public class ThirdPalindrome {

	public static void main(String[] args) {
	    
		//palindrome number are 121 and 125 is not palindrome
		
		int n=121;
		int og=n;
		int rev=0;
		
		while(n>0)
		{
			rev = rev*10 + n%10;
			n=n/10;
		}
		
		if(og==rev)
		{
			System.out.println("Number is palindrome");
		}
		else
		{
			System.out.println("Number is not palindrome");
		}

	}

}
